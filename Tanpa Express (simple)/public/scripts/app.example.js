class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");

    this.driver = document.getElementById("form_driverType");
    this.tanggal = document.getElementById("form_date");
    this.waktu = document.getElementById("form_time");
    this.penumpang = document.getElementById("form_jumlah");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {

    const node = document.createElement("div");
    node.className = "row iki atas";
    this.carContainerElement.className = "container cobalh";
    const newDiv = document.createElement("div");
    newDiv.className = "container hmm";
    node.appendChild(newDiv);

    const line = document.createElement("div");
    line.className = "row bawaah";
    this.carContainerElement.appendChild(line);

    console.log(this.penumpang.value);
    console.log(this.driver.value);
    console.log(this.tanggal.value);
    console.log(this.waktu.value);
    let d = (this.tanggal.value + "T" + this.waktu.value);
    let formdate = Date.parse(d)
    Car.list.forEach((car) => {
      let penumpang = this.penumpang.value
      let driber = this.driver.value
      if (driber == "true") {
        driber = true
      } else (
        driber = false
      )
      console.log(car.availableAt)

      // let tanggale = new Date(this.tanggal.value);

      let asalehe = Date.parse(car.availableAt)

      if (car.available == driber && asalehe >= formdate && car.capacity >= penumpang) {
        const cob = document.createElement("div");
        cob.className = "col-12 col-sm-6 col-md-4 col-xl-3 d-flex";
        // cob.style = "height : 500px"
        cob.innerHTML = car.render();
        line.appendChild(cob);
      }
      // let wagtu = car.availableAt
      // console.log(typeof(this.waktu.value));
      // console.log(typeof(this.tanggal.value));
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
