class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  // render() {
  //   return `
  //     <p>id: <b>${this.id}</b></p>
  //     <p>plate: <b>${this.plate}</b></p>
  //     <p>manufacture: <b>${this.manufacture}</b></p>
  //     <p>model: <b>${this.model}</b></p>
  //     <p>available at: <b>${this.availableAt}</b></p>
  //     <img src="${this.image}" alt="${this.manufacture}" width="64px">
  //   `;
  // }

  render() {
    return `
      <div class="card mt-3">
      <div class="card-img p-3">
      <img src="${this.image}" class="card-img-top rounded" alt="${this.manufacture}" weight="100%" height="160px">
      </div>
        <div class="card-body">
          <h5 class="nama-mobil"><b>${this.manufacture} ${this.model}</b></h5>
          <p class="harga-mobil"><b>Rp. ${this.rentPerDay} / hari</b></p>
          <p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis">${this.description}</p>
          <div class="tentang mt-1"><i class="mt-1 me-2 icon-mobil fa-solid bi-people">  ${this.capacity} People</i></div>
          <div class="tentang mt-1"><i class="mt-1 me-2 icon-mobil fa-solid bi-gear">  ${this.transmission}</i></div>
          <div class="tentang mt-1 mb-2"><i class="mt-1 me-2 icon-mobil fa-solid bi-calendar">  ${this.year}</i></div>
          <a href="#" class="btn btn-success d-flex justify-content-center tombol-mobil">Pilih Mobil</a>
        </div>
      </div>`;
  }

  // render() {
  //   return `
  //     <div class="card mt-3">
  //     <img class="img-fluid card-img" src="${this.image}" alt="${this.manufacture}">
  //     <div class="float-end  text-capitalize">
  //     <table class="p-3 m-2">
  //       <tr class="row-cols-4">
  //         <td class="col-2">Car Type</td>
  //         <td class="col-8">${this.manufacture}</td>
  //       </tr>
  //       <tr class="row-cols-4">
  //         <td class="col-2">model</td>
  //         <td class="col-8">${this.model}</td>
  //       </tr>
  //       <tr class="row-cols-4">
  //         <td class="col-2">plate</td>
  //         <td class="col-8">${this.plate}</td>
  //       </tr>
  //       <tr class="row-cols-4">
  //         <td class="col-2">id</td>
  //         <td class="col-8">${this.id}</td>
  //       </tr>
  //       <tr class="row-cols-4">
  //         <td class="col-2">available at</td>
  //         <td class="col-8">${this.availableAt}</td>
  //       </tr>
  //     </table>
  //       <button class="btn btn-success w-100 float-end">Pilih Mobil</button>
  //     </div>
  //     </div>`;
  // }
}
