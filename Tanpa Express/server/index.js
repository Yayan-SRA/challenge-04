const http = require('http');
const { PORT = 8000 } = process.env;
const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

function getHTML(htmlFileName) {
    const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
    return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
    switch (req.url) {
        case "/":
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200)
            res.end(getHTML("index.html"))
            break;
        case "/cari":
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200)
            res.end(getHTML("carimobil.html"))
            break;
        case "/contoh":
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200)
            res.end(getHTML("index.example.html"))
            break;
        case "/css/contoh":
            res.setHeader("Content-Type", "text/css");
            res.writeHead(200)
            res.end(getHTML("css/style.example.css"))
            break;
        case "/css/style":
            res.setHeader("Content-Type", "text/css");
            res.writeHead(200)
            res.end(getHTML("css/style.css"))
            break;
        case "/css/bootstrap":
            res.setHeader("Content-Type", "text/css");
            res.writeHead(200)
            res.end(getHTML("css/bootstrap.min.css"))
            break;
        case "/css/cari":
            res.setHeader("Content-Type", "text/css");
            res.writeHead(200)
            res.end(getHTML("css/cari.css"))
            break;
        case "/js/bootstrap":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("js/bootstrap.bundle.min.js"))
            break;
        case "/js/javas":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("js/script.js"))
            break;
        case "/js/binar":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("scripts/binar.js"))
            break;
        case "/js/app":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("scripts/app.example.js"))
            break;
        case "/js/car":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("scripts/car.example.js"))
            break;
        case "/js/main":
            res.setHeader("Content-Type", "text/js");
            res.writeHead(200)
            res.end(getHTML("scripts/main.example.js"))
            break;
        case "/icon/check":
            res.setHeader("Content-Type", "image/svg+xml");
            res.writeHead(200)
            res.end(getHTML("icon/check.svg"))
            break;
        case "/img/mobil":
            res.setHeader("Content-Type", "image/svg+xml");
            res.writeHead(200)
            res.end(getHTML("images/Mercedes Car EQC 300kW Edition - 900x598 1.svg"))
            break;
        case "/img/girl":
            res.setHeader("Content-Type", "image/svg+xml");
            res.writeHead(200)
            res.end(getHTML("img/Happy Girl.svg"))
            break;
        case "/img/tester1":
            const imagesTester = path.join(PUBLIC_DIRECTORY, "img/tester1.jpeg");
            const imagesT = fs.readFileSync(imagesTester, "");
            res.setHeader("Content-Type", "image/jpeg");
            res.writeHead(200);
            res.end(imagesT);
            break;
        case "/img/tester2":
            const imagesTester2 = path.join(PUBLIC_DIRECTORY, "img/tester2.jpg");
            const imagesT2 = fs.readFileSync(imagesTester2, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT2);
            break;
        case "/img/tester3":
            const imagesTester3 = path.join(PUBLIC_DIRECTORY, "img/tester2.jpg");
            const imagesT3 = fs.readFileSync(imagesTester3, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT3);
            break;
        case "/img/tester4":
            const imagesTester4 = path.join(PUBLIC_DIRECTORY, "img/tester4.jpg");
            const imagesT4 = fs.readFileSync(imagesTester4, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT4);
            break;
        case "/img/tester5":
            const imagesTester5 = path.join(PUBLIC_DIRECTORY, "img/tester5.jpg");
            const imagesT5 = fs.readFileSync(imagesTester5, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT5);
            break;
        case "/img/tester6":
            const imagesTester6 = path.join(PUBLIC_DIRECTORY, "img/tester6.jpg");
            const imagesT6 = fs.readFileSync(imagesTester6, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT6);
            break;
        case "/img/tester7":
            const imagesTester7 = path.join(PUBLIC_DIRECTORY, "img/tester7.jpg");
            const imagesT7 = fs.readFileSync(imagesTester7, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT7);
            break;
        case "/img/tester8":
            const imagesTester8 = path.join(PUBLIC_DIRECTORY, "img/tester8.jpg");
            const imagesT8 = fs.readFileSync(imagesTester8, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT8);
            break;
        case "/img/tester9":
            const imagesTester9 = path.join(PUBLIC_DIRECTORY, "img/tester9.jpg");
            const imagesT9 = fs.readFileSync(imagesTester9, "");
            res.setHeader("Content-Type", "image/jpg");
            res.writeHead(200);
            res.end(imagesT9);
            break;

        case "/img/logo":
            const imagesLogo = path.join(PUBLIC_DIRECTORY, "images/logo-binar.png");
            const imagesL = fs.readFileSync(imagesLogo, "");
            res.setHeader("Content-Type", "image/png");
            res.writeHead(200);
            res.end(imagesL);
            break;

        default:
            res.writeHead(404)
            res.end(getHTML("404.html"))
            break;
    }
}

const server = http.createServer(onRequest);

server.listen(PORT, '127.0.0.1', () => {
    console.log("Server sudah berjalan, silahkan buka http://127.0.0.1:%d", PORT);
})